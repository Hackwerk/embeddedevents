/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#ifndef EVENT_TIMER_H_INCLUDED
#define EVENT_TIMER_H_INCLUDED

#include "event.h"

void event_timer_tick(void); // should be fired every time the hardware timer ticks
void event_timer_register(event_id_t event_id, int ticks, int flags); // used to register a event timer
void event_timer_check(void); // should be fired in the main loop

#define EVENT_TIMER_FLAG_FIRE_ONCE 0b00000001

#endif // EVENT_TIMER_H_INCLUDED
