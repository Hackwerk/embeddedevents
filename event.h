/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED

#include "task.h"

//typedef void (*tEventHandler) (unsigned char *);
typedef unsigned int event_id_t;

void event_handler_register(task_handler_t event_handler, event_id_t event_id, task_priority_t task_priority);
void event_trigger(event_id_t event_id);


#endif // EVENT_H_INCLUDED
