/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#include <stdlib.h>
#include "event.h"

typedef struct event_handler_record
{
    struct event_handler_record *next_p;
    event_id_t event_id;
    task_priority_t task_priority;
    task_handler_t event_handler;
} event_handler_record_t;


static event_handler_record_t *event_handler_list = NULL;

void event_handler_register(task_handler_t event_handler, event_id_t event_id, task_priority_t task_priority)
{
    event_handler_record_t *new_handler_p;

    // create new handler in memory
    new_handler_p = malloc(sizeof(event_handler_record_t));
    if(new_handler_p == NULL) return;

    new_handler_p->event_id = event_id;
    new_handler_p->event_handler = event_handler;
    new_handler_p->task_priority = task_priority;
    new_handler_p->next_p = NULL;

    if(event_handler_list == NULL)
    {
        event_handler_list = new_handler_p;
    }
    else
    {
        // move handler to end of the list
        event_handler_record_t *active_record_p;
        active_record_p = event_handler_list;

        while(active_record_p->next_p != NULL)
        {
            active_record_p = active_record_p->next_p;
        }

        active_record_p->next_p = new_handler_p;
    }

}

void event_trigger(event_id_t event_id)
{
    if(event_handler_list == NULL) return;

    event_handler_record_t *active_record_p;

    active_record_p = event_handler_list;
    while(1)
    {
        // if we found a handler for the trigerd event
        if(active_record_p->event_id == event_id)
        {
            // queue the handler as a task
            task_queue(active_record_p->event_handler, active_record_p->task_priority);
        }
        if(active_record_p->next_p != NULL) active_record_p = active_record_p->next_p;
        else break;
    }

}
