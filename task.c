/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "task.h"

typedef struct task_record
{
    struct task_record *next_p;
    task_priority_t priority;
    task_handler_t handler;
} task_record_t;


static task_record_t *task_list = NULL;

void task_queue(task_handler_t handler, task_priority_t priority)
{
    /* Interrupts sould be disabled while running this function*/

    task_record_t *new_handler_p;

    new_handler_p = malloc(sizeof(task_record_t));
    if(new_handler_p == NULL) return;

    new_handler_p->priority = priority;
    new_handler_p->handler = handler;
    new_handler_p->next_p = NULL;

    // if the list is empty
    if(task_list == NULL)
    {
        task_list = new_handler_p;

        //printf("List was empty\n");
        return;
    }

    // if the priority of this task is higher then te first task in the queue
    if(priority > task_list->priority)
    {
        new_handler_p->next_p = task_list;
        task_list = new_handler_p;
        //printf("Prio was higher then first task in queue\n");
        return;
    }

    // place the task in the right place in the list
    task_record_t *active_record_p;
    active_record_p = task_list;
    while(1)
    {
        // if all other priorities were higher, add it to the bottom
        if(active_record_p->next_p == NULL)
        {
            active_record_p->next_p = new_handler_p;
            //printf("Task added to bottom\n");
            return;
        }


        // if the priority of this record is higher then the already queued one
        if(priority > active_record_p->next_p->priority)
        {
            // add the record to the queue
            new_handler_p->next_p = active_record_p->next_p;
            active_record_p->next_p = new_handler_p;

            //printf("Prio was higher then already queued task\n");
            return;
        }



        active_record_p = active_record_p->next_p;
    }
}

void task_run(void)
{
    // return if the list is empty
    if(task_list == NULL) return;

    // run the handler
    task_list->handler();

    // store record pointer
    task_record_t *old_record_p;
    old_record_p = task_list;

    // set the next task as the top of the list
    task_list = task_list->next_p;

    // remove the old record from memory
    free(old_record_p);
}
