/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "event_timer.h"

static int tick_count = 0;

typedef struct event_timer_record
{
    struct event_timer_record *next_p;
    int next_tick_count;
    int ticks;
    int flags;
    event_id_t event_id;

} event_timer_record_t;

static event_timer_record_t *event_timer_list;

void event_timer_reset(event_timer_record_t *timer_p);


void event_timer_tick(void)
{
    tick_count++;
}

void event_timer_register(event_id_t event_id, int ticks, int flags)
{
    // create new handler in memory
    event_timer_record_t *new_handler_p;
    new_handler_p = malloc(sizeof(event_timer_record_t));
    if(new_handler_p == NULL) return;

    new_handler_p->event_id = event_id;
    new_handler_p->ticks = ticks;
    new_handler_p->next_tick_count = 0;
    new_handler_p->next_p = NULL;
    new_handler_p->flags = flags;


    if(event_timer_list == NULL)
    {
        event_timer_list = new_handler_p;
    }
    else
    {
        // put timer on the end of the list
        event_timer_record_t *active_record_p;
        active_record_p = event_timer_list;

        while(active_record_p->next_p != NULL)
        {
            active_record_p = active_record_p->next_p;
        }

        active_record_p->next_p = new_handler_p;
    }
}

void event_timer_check(void)
{
    if (event_timer_list == NULL) return;
    
    event_timer_record_t *active_record_p = event_timer_list;
    event_timer_record_t *previous_record_p = NULL;
    while(1)
    {
        // check if the timer ticked
        if(tick_count >= active_record_p->next_tick_count)
        {
            // if this timer is set as fire once, remove it
            if(active_record_p->flags & EVENT_TIMER_FLAG_FIRE_ONCE)
            {
                event_trigger(active_record_p->event_id);

                // if this is the first iteration
                if(previous_record_p == NULL)
                {
                    event_timer_list = active_record_p->next_p;
                    free(active_record_p);

                    active_record_p = event_timer_list;
                    continue;
                }
                else
                {
                    previous_record_p->next_p = active_record_p->next_p;
                    free(active_record_p);
                    active_record_p = previous_record_p;
                }

            }
            else
            {
                event_timer_reset(active_record_p);// reset timer
                event_trigger(active_record_p->event_id); // trigger the event
            }
        }

        // check if there is a next list item
        if(active_record_p->next_p == NULL)
        {
            break;
        }
        else
        {
            previous_record_p = active_record_p;
            active_record_p = active_record_p->next_p;
        }
    }
}

void event_timer_reset(event_timer_record_t *timer_p)
{
    timer_p->next_tick_count = tick_count + timer_p->ticks;
}
