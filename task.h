/*
 *	Copyright 2014 Martijn Stommels
 *
 *	This file is part of EmbeddedEvents.
 *
 *	EmbeddedEvents is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbeddedEvents is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with Foobar.  If not, see <http://www.gnu.org/licenses/>. 
 * 
 */ 

#ifndef TASK_H_INCLUDED
#define TASK_H_INCLUDED


#define TASK_PRIORITY_LOW 0
#define TASK_PRIORITY_MEDIUM 127
#define TASK_PRIORITY_HIGH 255

typedef void (*task_handler_t) (void);
typedef unsigned char task_priority_t;

void task_queue(task_handler_t, task_priority_t);
void task_run(void);


#endif // TASK_H_INCLUDED
